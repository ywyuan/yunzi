package com.oreo.yunzi.metro;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Station
 *
 * @author yuan
 * @since 2021/12/19 09:21
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Station {
    private Long id;
    private String name;
    private String line;

    public String info() {
        return line+":"+name;
    }
}
