package com.oreo.yunzi.metro;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Path
 *
 * @author yuan
 * @since 2021/12/19 09:37
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Path {
    private List<Station> stations;
    private List<String> lines;

    public void addStation(Station station) {
        if (null == stations) {
            stations = new ArrayList<>();
        }
        stations.add(station);
    }

    public Path copy() {
        List<Station> copyStations = new ArrayList<>();
        if (!CollectionUtils.isEmpty(stations)) {
            for (Station station : stations) {
                copyStations.add(station);
            }
        }


        List<String> copyLines = new ArrayList<>();
        if (!CollectionUtils.isEmpty(lines)) {
            for (String line : lines) {
                copyLines.add(line);
            }
        }

        return new Path(copyStations, copyLines);
    }

    public void print() {
        StringBuilder sb = new StringBuilder();

        int size = stations.size();
        for (int i = 0; i < size; i++) {
            Station station = stations.get(i);
            sb.append(station.info());
            if (i < size - 1) {
                sb.append(" ==> ");
            }
        }

        System.out.println(sb.toString());
    }
}
