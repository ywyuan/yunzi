package com.oreo.yunzi.metro;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Decider
 *
 * @author yuan
 * @since 2021/12/19 09:25
 */
public class Decider {


    private static final MetroMap metro = MetroMap.mock2();

    public static void main(String[] args) {

        List<Path> paths = new Decider().choosePath(100L, 409L);
        for (Path path : paths) {
            path.print();
        }
    }


    public List<Path> choosePath(Long startId, Long endId) {

        Station start = metro.getStationById(startId);
        Station end = metro.getStationById(endId);

        System.out.printf("%n from %s to %s %n", start.info(), end.info());
        Path path = Path.builder()
                .stations(new ArrayList<>())
                .build();

        return searchPath(start, end, path);
    }

    private List<Path> searchPath(Station cur, Station end, Path path) {
        List<Station> stations = sameStations(cur);
        Long endId = end.getId();
        List<Path> paths = new ArrayList<>();
        for (Station station : stations) {

            Path copyPath = path.copy();
            copyPath.addStation(station);

            // 到达站点
            if (station.getId().equals(endId)) {
                paths.add(copyPath);
                continue;
            }

            List<Station> nextStations = nextStations(station, copyPath);
            for (Station nextStation : nextStations) {

                List<Path> subPaths = searchPath(nextStation, end, copyPath);
                paths.addAll(subPaths);
            }
        }

        return paths;
    }


    /**
     * 多线交汇点、可换乘站点
     *
     * @return
     */
    private List<Station> sameStations(Station s) {
        // 包含 当前站点
        return metro.getSameStations(s.getId());
    }

    /**
     * 下一可达站点 线
     *
     * @param start
     * @return
     */
    private List<Station> nextStations(Station start, Path path) {

        // 走过的 + 同站id
        Set<Long> passedByStation = new HashSet<>();
        for (Station s : path.getStations()) {
            passedByStation.addAll(sameStations(s)
                    .stream()
                    .map(Station::getId)
                    .collect(Collectors.toSet()));
        }

        // 下一可达站点
        List<Station> nextStations = metro.getNextStations(start.getId());

        List<Station> filtered = new ArrayList<>();
        for (Station nextStation : nextStations) {

            // 过滤已走过的路线
            if (passedByStation.contains(nextStation.getId())) {
                continue;
            }
            filtered.add(nextStation);
        }
        return filtered;
    }
}
