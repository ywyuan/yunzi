package com.oreo.yunzi.metro;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.util.CollectionUtils;

import java.util.*;
import java.util.stream.Collectors;

/**
 * MetroMap
 *
 * @author yuan
 * @since 2021/12/19 10:23
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class MetroMap {

    /**
     * Stations  id name line
     */
    List<Station> stations;
    /**
     * StationLink 可达分析 from to A-->B A-->C A-->D  B-->A
     */
    Map<Long, List<Long>> stationLink;
    /**
     * TransStation 多线交叉 换乘点 A(L1)-->A(L2)  A(L1)-->A(L3) A(L2)-->A(L1)
     */
    Map<Long, List<Long>> transStation;


    public static MetroMap mock2() {
        MetroMap metroMap = MetroMap.builder()
                .stations(new ArrayList<>())
                .stationLink(new HashMap<>())
                .transStation(new HashMap<>())
                .build();
        metroMap.mockLine("L1", 100L, "军事博物馆", "木樨地", "复兴门", "西单", "天安门西", "天安门东", "王府井", "东单", "建国门", "永安里", "国贸");
        metroMap.mockLine("L2", 200L, "复兴门", "阜成门", "车公庄", "西直门", "积水潭", "鼓楼大街", "安定门", "雍和宫", "东直门", "朝阳门", "建国门", "崇文门", "前门", "宣武门", "复兴门");
        metroMap.mockLine("L4", 400L, "菜市口", "宣武门", "西单", "灵境胡同", "西四", "平安里", "新街口", "西直门", "动物园", "国家图书馆");

        // mock trans
        metroMap.calculateTransByName();
        return metroMap;
    }

    public void mockLine(String line, Long baseId, String... names) {

        List<Station> lineStations = new ArrayList<>();

        Map<String, Station> nameStations = new HashMap<>();

        // mock station
        for (String name : names) {

            if (nameStations.containsKey(name)) {
                continue;
            }

            Long id = baseId++;
            Station station = new Station(id, name, line);

            System.out.println(station.info() + "-->id=" + station.getId());
            lineStations.add(station);
            nameStations.put(name, station);
        }

        stations.addAll(lineStations);

        // mock link
        Map<Long, List<Long>> lineStationLink = new HashMap<>();
        int size = names.length;
        List<String> nameArr = Arrays.asList(names);
        for (int i = 0; i < size - 1; i++) {
            String name = nameArr.get(i);
            String nextName = nameArr.get(i + 1);

            Station station = nameStations.get(name);
            Station nextStation = nameStations.get(nextName);

            putLink(lineStationLink, station.getId(), nextStation.getId());
            putLink(lineStationLink, nextStation.getId(), station.getId());
        }

        stationLink.putAll(lineStationLink);
    }

    public void calculateTransByName() {

        int size = stations.size();
        for (int i = 0; i < size; i++) {
            Station is = stations.get(i);
            for (int j = i + 1; j < size; j++) {
                Station js = stations.get(j);
                if (is.getName().equals(js.getName())) {

                    putLink(transStation, is.getId(), js.getId());
                    putLink(transStation, js.getId(), is.getId());
                }
            }

        }
    }

    public static void putLink(Map<Long, List<Long>> stationLink, Long from, Long to) {
        List<Long> longs = stationLink.computeIfAbsent(from, k -> new ArrayList<>());
        longs.add(to);
    }

    public List<Station> getSameStations(Long id) {

        List<Long> longs = transStation.get(id);
        if (CollectionUtils.isEmpty(longs)) {
            longs = new ArrayList<>();
        }
        // 包含 当前站点
        longs.add(id);


        return getStationByIds(longs);
    }


    public List<Station> getNextStations(Long id) {

        List<Long> longs = stationLink.get(id);
        if (CollectionUtils.isEmpty(longs)) {
            longs = new ArrayList<>();
        }

        return getStationByIds(longs);
    }


    public Station getStationById(Long id) {
        for (Station station : stations) {
            if (id.equals(station.getId())) {
                return station;
            }
        }
        return null;
    }

    public List<Station> getStationByIds(List<Long> ids) {

        Set<Long> longSet = ids.stream().collect(Collectors.toSet());

        List<Station> res = new ArrayList<>();
        for (Station station : stations) {
            if (longSet.contains(station.getId())) {
                res.add(station);
            }
        }
        return res;
    }


}
