package com.oreo.yunzi.test.mapstruct;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * <Description>
 *
 * @author yuan
 * @since 2020/11/18 10:01
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Car {

    private String make;
    private int numberOfSeats;
    private CarType type;
    private Integer wrap;

    private Date estimater;

}
