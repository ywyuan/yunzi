package com.oreo.yunzi.test.mapstruct;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * <Description>
 *
 * @author yuan
 * @since 2020/11/18 10:02
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CarDto {
    private String make;
    private int seatCount;
    private String type;

    private int wrap;

}
