package com.oreo.yunzi.test.bean;

import com.oreo.yunzi.util.PrintUtil;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.beans.factory.config.InstantiationAwareBeanPostProcessor;
import org.springframework.stereotype.Component;

/**
 * <Description>
 *
 * @author yuan
 * @since 2020/12/6 17:41
 */
@Component
public class TestBean implements InitializingBean, DisposableBean
        , BeanPostProcessor
//        , InstantiationAwareBeanPostProcessor
{
    private String name;

    @Override
    public void afterPropertiesSet() throws Exception {
        PrintUtil.print("InitializingBean.afterPropertiesSet");
    }


    @Override
    public void destroy() throws Exception {
        PrintUtil.print("DisposableBean.destroy");
    }

    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
        PrintUtil.print("BeanPostProcessor.postProcess[Before]Initialization");
        return bean;
    }

    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        PrintUtil.print("BeanPostProcessor.postProcess[After]Initialization");

        return bean;
    }
}
