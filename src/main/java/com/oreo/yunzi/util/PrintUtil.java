package com.oreo.yunzi.util;

import org.slf4j.helpers.FormattingTuple;
import org.slf4j.helpers.MessageFormatter;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * PrintUtil
 *
 * @author yuan
 * @since 2020-07-26 22:58
 */
public class PrintUtil {

    /************************************** format ********************************/

    public static void dotLongRow() {
        print("........................................................................................................");
    }

    public static void dotRow() {
        print("....................................................");
    }

    public static void lineRow() {
        print("\n----------------------------------------------------\n");
    }

    public static void blankRow() {
        print("\n");
    }

    /**
     * 格式化输出字符串
     *
     * @param s
     * @param objects
     * @return
     */
    public static String format(String s, Object... objects) {
        FormattingTuple tuple = MessageFormatter.arrayFormat(s, objects);

        return tuple.getMessage();
    }

    /************************************** map ********************************/

    /**
     * 格式化打印字符串
     *
     * @param s
     * @param objects
     */
    public static void pretty(String s, Object... objects) {
        print(format(s, objects));
    }

    /**
     * print array
     *
     * @param arr
     * @param <T>
     */
    public static <T> void array(T[] arr) {
        dotRow();
        print(Arrays.toString(arr));
    }


    /**
     * map list
     *
     * @param list
     * @param <T>
     */
    public static <T> void list(List<T> list) {
        dotRow();
        for (T s : list) {
            print(s);
        }
    }

    /**
     * map map
     *
     * @param map
     * @param <K>
     * @param <T>
     */
    public static <K, T> void map(Map<K, T> map) {
        dotRow();
        for (Map.Entry<K, T> ktEntry : map.entrySet()) {
            pretty("key: {} ==> value: {}", ktEntry.getKey(), ktEntry.getValue());
        }
    }

    /************************************ base ********************************/

    public static void print(Object obj) {
        System.out.println(obj);
    }

    /**
     * 带上时间戳
     *
     * @param obj
     * @param startMills
     */
    public static void printNowObj(Object obj, long startMills) {
        long now = System.currentTimeMillis();
        long cur = now - startMills;
        System.out.println("curMills:" + cur + "\t" + obj);
    }
}
