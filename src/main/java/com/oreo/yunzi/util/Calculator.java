package com.oreo.yunzi.util;

import java.math.BigDecimal;

/**
 * <Description>
 *
 * @author yuan
 * @since 2020-07-26 22:53
 */
public class Calculator {

    public static float keep2(float f) {
        return keepN(2,f);
    }

    public static float keepN(int n, float f) {
        BigDecimal bd = new BigDecimal(f);
        //保留小数点后n位，并四舍五入
        bd = bd.setScale(n, BigDecimal.ROUND_HALF_UP);
        // 将BigDecimal类型转换为float类型
        return bd.floatValue();
    }

    public int add(int a, int b) {
        return a + b;
    }

    public int minus(int a, int b) {
        return a - b;
    }
}
