package com.oreo.yunzi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class YunziApplication {

	public static void main(String[] args) {
		SpringApplication.run(YunziApplication.class, args);
	}

}
