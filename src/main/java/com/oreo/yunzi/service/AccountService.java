package com.oreo.yunzi.service;

/**
 * AccountService
 *
 * @author yuan
 * @since 2020-07-26 23:12
 */
public interface AccountService {

    boolean isEvenNumber(int x);
}
