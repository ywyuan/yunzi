package com.oreo.yunzi.service;

/**
 * ItemService
 *
 * @author yuan
 * @since 2020-07-26 23:23
 */
public interface ItemService {

    int countItem(int type);
}
