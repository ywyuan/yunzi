package com.oreo.yunzi.service.impl;

import com.oreo.yunzi.service.AccountService;
import org.springframework.stereotype.Service;

/**
 * <Description>
 *
 * @author yuan
 * @since 2020-07-26 23:12
 */
@Service
public class AccountServiceImpl implements AccountService {

    @Override
    public boolean isEvenNumber(int x) {
        return (x & 1) == 0;
    }
}
