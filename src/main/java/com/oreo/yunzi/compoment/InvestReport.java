package com.oreo.yunzi.compoment;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 投资报告
 *
 * @author yuan
 * @since 2020/11/15 12:52
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class InvestReport {
    private Item money = new Item();
    private Item cost = new Item();
    private Item profit = new Item();
    private Item profitRate = new Item();
    private Item spendDays = new Item();

    public void processEntry(InvestReturn entry) {
        float money = entry.getTotalMoney();
        float holdCost = entry.getHoldCost();
        float profit = entry.getProfit();
        float profitRate = entry.getProfitRate();
        long spendDays = entry.getSpendDays();

        compareAndCal(this.getMoney(), money);
        compareAndCal(this.getCost(), holdCost);
        compareAndCal(this.getProfit(), profit);
        compareAndCal(this.getProfitRate(), profitRate);
        compareAndCal(this.getSpendDays(), spendDays);
    }

    public void calculateAvg(int size) {

        calItemAvg(this.getMoney(), size);
        calItemAvg(this.getCost(), size);
        calItemAvg(this.getProfit(), size);
        calItemAvg(this.getSpendDays(), size);

        // 平均收益率
        Item profitRate = this.getProfitRate();
        profitRate.avg = 100 * getProfit().sum / getCost().sum;
    }

    private void calItemAvg(Item item, int size) {
        item.avg = item.sum / size;
    }

    private void compareAndCal(Item item, float f) {

        // 初始
        if (null == item.min) {
            item.min = f;
        }
        if (null == item.max) {
            item.max = f;
        }

        // 迭代比较
        if (item.min > f) {
            item.min = f;
        }
        if (item.max < f) {
            item.max = f;
        }
        item.sum += f;
    }

    @Data
    public static class Item {
        private float sum;
        private Float min;
        private Float max;
        private float avg;
    }
}
