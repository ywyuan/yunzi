package com.oreo.yunzi.compoment;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 投资策略
 *
 * @author yuan
 * @since 2020/11/12 20:10
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public abstract class InvestStrategy {

    /**
     * 最大投资额
     */
    protected float totalInvestAmount;
    /**
     * 期望收益率
     */
    protected float expectProfitPercent;


    public abstract boolean buyOrNot(FundMocker fundMocker);

    public boolean sellOrNot(FundMocker fundMocker) {

        float profit = fundMocker.holdProfit();
        float holdCost = fundMocker.getHoldCost();
        if (holdCost == 0) {
            return false;
        }

        float rate = profit / holdCost;
        float ratePercent = rate * 100;

        if (ratePercent > expectProfitPercent) {
//            float totalMoney = fundMocker.sellPart(1, 1);
//            long daysFromBegin = fundMocker.getDaysFromBegin();
//
//            InvestReturn investReturn = new InvestReturn(totalMoney,holdCost,profit,ratePercent,daysFromBegin);
//            System.out.println("\n结果公布：" + investReturn);

            return true;
        }

        // todo 止损卖出？20%

        return false;
    }


    public InvestReturn doSell(FundMocker fundMocker) {

        long daysFromBegin = fundMocker.getDaysFromBegin();
        float profit = fundMocker.holdProfit();
        float holdCost = fundMocker.getHoldCost();
        if (holdCost == 0) {
            return new InvestReturn(0, 0, 0, 0, daysFromBegin);
        }

        float rate = profit / holdCost;
        float ratePercent = rate * 100;

        float totalMoney = fundMocker.sellPart(1, 1);

        InvestReturn investReturn = new InvestReturn(totalMoney, holdCost, profit, ratePercent, daysFromBegin);
        System.out.println("本次卖出公布：" + investReturn + "\n");

        return investReturn;
    }
}
