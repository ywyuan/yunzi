package com.oreo.yunzi.compoment;

import lombok.Data;

/**
 * RegularStrategy--定期定额
 *
 * @author yuan
 * @since 2020/11/9 20:07
 */
@Data
public class RegularStrategy extends InvestStrategy {

    /**
     * 每次投资金额
     */
    private float investBaseAmount;
    /**
     * 投资周期：间隔天数
     */
    private int intervalDays;

    public RegularStrategy(float totalInvestAmount, float expectProfitPercent, float investBaseAmount, int intervalDays) {
        super(totalInvestAmount, expectProfitPercent);
        this.investBaseAmount = investBaseAmount;
        this.intervalDays = intervalDays;
    }

    @Override
    public boolean buyOrNot(FundMocker fundMocker) {
        long daysFromBegin = fundMocker.getDaysFromBegin();
        boolean period = daysFromBegin % intervalDays == 0;
        boolean haveMoney = fundMocker.getHoldCost() < getTotalInvestAmount();

        boolean buy = period && haveMoney;
        if (buy) {
            fundMocker.buy(investBaseAmount);
        }

        return buy;
    }

}
