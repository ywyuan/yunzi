package com.oreo.yunzi.compoment;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * <Description>
 *
 * @author yuan
 * @since 2020/11/15 12:40
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class InvestReturn {
    private float totalMoney;
    private float holdCost;
    private float profit;
    private float profitRate;
    private long spendDays;

    @Override
    public String toString() {
        return "InvestReturn{" +
                "totalMoney=" + totalMoney +
                ", holdCost=" + holdCost +
                ", profit=" + profit +
                ", profitRate=" + profitRate +
                "%, spendDays=" + spendDays +
                '}';
    }
}
