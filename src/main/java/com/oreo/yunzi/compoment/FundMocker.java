package com.oreo.yunzi.compoment;

import com.oreo.yunzi.util.Calculator;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.util.Assert;

import java.util.Random;

/**
 * 折线模拟器
 *
 * @author yuan
 * @since 2020/11/9 20:06
 */
@Data
@NoArgsConstructor
public class FundMocker {
    private static final boolean logInOutInfo = false;
//    private static final boolean logInOutInfo = true;

    private static final Random R = new Random();
    private long daysFromBegin;
    /**
     * 名称
     */
    private String name;

    /**
     * 价格
     */
    private float price = 1;

    /**
     * 最大振幅 10% 20%
     */
    private float maxAmplitude = 5F;

    /**
     * 持有份额
     */
    private float hold;
    /**
     * 持有成本
     */
    private float holdCost;

    public FundMocker(String name) {
        this.name = name;
    }

    /**
     * 模拟价格波动曲线
     *
     * @return
     */
    public void nextPrice() {
        float randomFloat = R.nextFloat();
        // ±10
        double changeRate = (randomFloat - 0.5) * 2 * maxAmplitude;
        float changeRateKeep2 = Calculator.keep2((float) changeRate);
//        System.out.println("changeRate=" + changeRateKeep2 + "%");

        price = Calculator.keep2(price * changeRateKeep2 / 100 + price);

        Assert.isTrue(price > 0, "wow~~破产了~~price=" + price);

        daysFromBegin++;
    }


    /**
     * 当前价值
     *
     * @return
     */
    public float currentHoldValue() {
        return Calculator.keep2(hold * price);
    }

    /**
     * 持仓获利
     *
     * @return
     */
    public float holdProfit() {
        return currentHoldValue() - holdCost;
    }

    /**
     * 执行买入
     *
     * @param amount 买入金额
     */
    public void buy(float amount) {
        if (amount<=0){
            return;
        }

        /**
         * 买入金额
         */
        holdCost += amount;

        /**
         * 买入份额
         */
        hold += Calculator.keepN(4, amount / price);


        if (logInOutInfo) {
            System.out.print("第" + daysFromBegin + "天:");
            System.out.println("<==买入" + amount + " " + this.toString());
        }
    }

    /**
     * 卖出 dividePart 分之 sellPart
     * dividePart=4，sellPart=3，则表示卖出3/4
     *
     * @param dividePart
     * @param sellPart
     * @return 卖出部分金额
     */
    public float sellPart(Integer dividePart, Integer sellPart) {
        Assert.isTrue(dividePart > 0 && sellPart > 0 && dividePart >= sellPart, "参数不满足：0 < sellPart <= dividePart");

        // 卖出份额
        float sellHold = hold * sellPart / dividePart;
        hold -= sellHold;

        // 卖出部分对应的成本
        float sellCost = holdCost * sellPart / dividePart;
        holdCost -= sellCost;

        if (logInOutInfo) {
            System.out.print("第" + daysFromBegin + "天:");
            System.out.println("======>卖出" + dividePart + "分之" + sellPart + " " + this.toString());
        }

        // TODO 暂不计算手续费
        return sellHold * price;
    }

    @Override
    public String toString() {
        return "FundMocker{" +
                "price=" + price +
                ", hold=" + hold +
                ", holdCost=" + holdCost +
                '}';
    }
}
