package com.oreo.yunzi.compoment;

import lombok.Data;

/**
 * todo 定期不定额90% 120%。。。
 *
 * @author yuan
 * @since 2020/11/12 20:47
 */
@Data
public class VaryAmountStrategy extends InvestStrategy {

    /**
     * 每次投资基数
     */
    private float investBaseAmount;

    /**
     * 投资周期：间隔天数
     */
    private int intervalDays;


    public VaryAmountStrategy(float totalInvestAmount, float expectProfitPercent, float investBaseAmount, int intervalDays) {
        super(totalInvestAmount, expectProfitPercent);
        this.investBaseAmount = investBaseAmount;
        this.intervalDays = intervalDays;
    }

    //    public static void main(String[] args) {
//        VaryAmountStrategy strategy = new VaryAmountStrategy();
//        strategy.getInvestWaveCoefficient(-1);
//        strategy.getInvestWaveCoefficient(-0.55F);
//        strategy.getInvestWaveCoefficient(-0.15F);
//        strategy.getInvestWaveCoefficient(-0.05F);
//        strategy.getInvestWaveCoefficient(0.05F);
//        strategy.getInvestWaveCoefficient(0.25F);
//        strategy.getInvestWaveCoefficient(0.45F);
//        strategy.getInvestWaveCoefficient(0.55F);
//        strategy.getInvestWaveCoefficient(0.65F);
//
//    }

    @Override
    public boolean buyOrNot(FundMocker fundMocker) {
        long daysFromBegin = fundMocker.getDaysFromBegin();
        boolean period = daysFromBegin % intervalDays == 0;
        boolean haveMoney = fundMocker.getHoldCost() < getTotalInvestAmount();

        boolean buy = period && haveMoney;
        if (buy) {

            // 当前价格
            float price = fundMocker.getPrice();
            // 初始不调控
            float beta = 1;

            float holdCost = fundMocker.getHoldCost();
            if (holdCost > 0) {
                // 持仓成本价格
                float holdCostPrice = fundMocker.getHoldCost() / fundMocker.getHold();

                // todo 投资额计算 阶梯化 + 设置边界
                beta = getInvestWaveCoefficient(price, holdCostPrice);
            }

            float investAmount = investBaseAmount * beta;
            fundMocker.buy(investAmount);
        }

        return buy;
    }

    /**
     * 计算调控系数
     *
     * @param curPrice  当前价格
     * @param holdPrice 持有价格
     * @return
     */
    private float getInvestWaveCoefficient(float curPrice, float holdPrice) {

        //  价格波动系数 (-1,+∞)
        float alpha = (curPrice - holdPrice) / holdPrice;
        int waveCoefficient = (int) (alpha * 100);

        // 上涨就加速减少买入
        if (waveCoefficient>20){
            return 0;
        }else if (waveCoefficient>10){
            return 0.5F;
        }else if (waveCoefficient>5){
            return 0.7F;

            // 下跌
        }else if (waveCoefficient>-5){
            return 1F;
        }else if (waveCoefficient>-10){
            return 1.5F;
        }else if (waveCoefficient>-20){
            return 2F;
        }else if (waveCoefficient>-30){
            return 4F;
        }else if (waveCoefficient>-40){
            return 8F;
        }else {
            return 8F;
        }
    }

}
