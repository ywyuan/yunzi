package com.oreo.yunzi.compoment;


import com.alibaba.fastjson.JSON;

import java.util.ArrayList;
import java.util.List;

/**
 * Estimater
 *
 * @author yuan
 * @since 2020/11/10 10:31
 */
public class Estimater {


    public static void main(String[] args) {
//        mockRegularStrategy();
        mockVaryAmountStrategy();
    }

    public static void mockVaryAmountStrategy() {
        VaryAmountStrategy varyAmountStrategy = new VaryAmountStrategy(100000, 20, 500, 5);
//        mockStrategy(varyAmountStrategy);
        multiMock(varyAmountStrategy, 100);
    }

    public static void mockRegularStrategy() {
        RegularStrategy regularStrategy = new RegularStrategy(100000, 20, 500, 5);

        multiMock(regularStrategy, 100);
    }

    /**
     * mock 不同的策略
     *
     * @param strategy
     */
    public static InvestReturn mockStrategy(InvestStrategy strategy) {
        FundMocker fundMocker = new FundMocker("KKCFZY");

        for (; ; ) {
            /**
             * 检查是否需要卖出
             */
            if (strategy.sellOrNot(fundMocker)) {
                // 卖出
                return strategy.doSell(fundMocker);
            }

            /**
             * 模拟下一天的价格
             */
            fundMocker.nextPrice();

            /**
             * 检查是否需要买入
             */
            strategy.buyOrNot(fundMocker);


            float profit = fundMocker.holdProfit();
            float rate = profit / fundMocker.getHoldCost();
            float ratePercent = rate * 100;

            long daysFromBegin = fundMocker.getDaysFromBegin();

            boolean curFail = ratePercent < -40;
            if (curFail) {
                System.out.println("第" + daysFromBegin + "天: rate=" + ratePercent + "% profit=" + profit + "太惨了~~");
                return strategy.doSell(fundMocker);
            }
            if (daysFromBegin == 1200) {
                System.out.println("第" + daysFromBegin + "天: rate=" + ratePercent + "% profit=" + profit + "等不及了~~");
                return strategy.doSell(fundMocker);
            }
        }
    }

    /**
     * 多次mock
     *
     * @param strategy
     * @param mockTimes
     */
    private static void multiMock(InvestStrategy strategy, int mockTimes) {
        List<InvestReturn> returnList = new ArrayList<>();
        for (int i = 0; i < mockTimes; i++) {
            InvestReturn investReturn = mockStrategy(strategy);
            returnList.add(investReturn);
        }

        InvestReport report = statisticAnalyseReport(returnList);
        String reportStr = JSON.toJSONString(report);

        String strategyName = strategy.getClass().getSimpleName();
        System.out.println("===>汇总分析" + strategyName + "  " + reportStr);

    }

    /**
     * 汇总分析
     *
     * @param list
     * @return
     */
    private static InvestReport statisticAnalyseReport(List<InvestReturn> list) {
        InvestReport report = new InvestReport();

        for (InvestReturn investReturn : list) {
            report.processEntry(investReturn);
        }
        report.calculateAvg(list.size());

        return report;
    }

}
