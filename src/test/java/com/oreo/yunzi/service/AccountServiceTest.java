package com.oreo.yunzi.service;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

/**
 * <Description>
 *
 * @author yuan
 * @since 2020-07-26 23:16
 */
@SpringBootTest
class AccountServiceTest {

    @Autowired
    AccountService accountService;

    @MockBean
    ItemService itemService;

    @Test
    void testEvenNumber() {
        boolean res = accountService.isEvenNumber(2);
        Assertions.assertEquals(true, res);
    }


}