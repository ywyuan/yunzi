package com.oreo.yunzi.service.impl;

import com.oreo.yunzi.service.ItemService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.SpyBean;

import static org.mockito.Mockito.when;

/**
 * <Description>
 *
 * @author yuan
 * @since 2020-07-26 23:24
 */
@SpringBootTest
class ItemServiceImplTest {

//    @Autowired
//    ItemService itemService;

//    @MockBean
//    ItemService itemService;

    @SpyBean
    ItemService itemService;

    @Test
    void countItem() {
        when(itemService.countItem(1)).thenReturn(3);

        int x1= itemService.countItem(1);
        int x2= itemService.countItem(2);
        Assertions.assertEquals(3,x1);
        Assertions.assertEquals(5,x2);
    }
}