package com.oreo.yunzi.util;

import org.junit.jupiter.api.*;

/**
 * CalculatorTest
 *
 * @author yuan
 * @since 2020-07-26 22:54
 */
class CalculatorTest {

    private static Calculator calculator;

    @BeforeAll
    public static void beforeAll() {
        calculator = new Calculator();
        PrintUtil.print("beforeAll ... ");
    }

    @AfterAll
    public static void afterAll() {
        PrintUtil.print("afterAll ... ");
    }

    @BeforeEach
    public void beforeEach() {
        PrintUtil.print("beforeEach ... ");
    }

    @Test
    void add() {
        int sum = calculator.add(3, 4);
        Assertions.assertEquals(7, sum);
        PrintUtil.print("test add ... ");
    }

    @Test
    void minus() {
        int delta = calculator.minus(3, 4);
        Assertions.assertEquals(-1, delta);
        PrintUtil.print("test minus ... ");
    }

    @AfterEach
    public void afterEach() {
        PrintUtil.print("afterEach ... ");
    }


}