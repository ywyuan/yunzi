package com.oreo.yunzi.test.mapstruct;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Date;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * <Description>
 *
 * @author yuan
 * @since 2020/11/18 10:06
 */
@SpringBootTest
class CarMapperTest {

    @Test
    public void shouldMapCarToDto() {
        //given
        Car car = new Car("Morris", 5, CarType.SEDAN, null, new Date());

        //when
        CarDto carDto = CarMapper.INSTANCE.carToCarDto(car);
        CarDto carDto2 = CarMapper.INSTANCE.carToCarDto(null);

        //then
        assertThat(carDto).isNotNull();
        assertThat(carDto.getMake()).isEqualTo("Morris");
        assertThat(carDto.getSeatCount()).isEqualTo(5);
        assertThat(carDto.getType()).isEqualTo("SEDAN");
    }

}